"use strict";
$(document).ready(function () {
    apiGetDrinkList();

    $("#btn-small").click(changeColorButtonCombo);
    $("#btn-medium").click(changeColorButtonCombo);
    $("#btn-large").click(changeColorButtonCombo);

    $("#btn-seafood").click(changeColorButtonPizza);
    $("#btn-hawaiian").click(changeColorButtonPizza);
    $("#btn-bacon").click(changeColorButtonPizza);

    $("#btn-send").click(sendClick);
    $("#btn-confirm").click(confirmClick);
});

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365";
var gCOMBO_SELECTED = { kichCo: "", duongKinh: 0, suon: 0, salad: 0, soLuongNuoc: 0, price: 0 };
var gPIZZA_SELECTED = { loaiPizza: "", detail: "" };

// các combo Pizza
const gCOMBO_SMALL = { kichCo: "Small", duongKinh: 20, suon: 2, salad: 200, soLuongNuoc: 2, price: 150 };
const gCOMBO_MEDIUM = { kichCo: "Medium", duongKinh: 25, suon: 4, salad: 300, soLuongNuoc: 3, price: 200 };
const gCOMBO_LARGE = { kichCo: "Large", duongKinh: 30, suon: 8, salad: 500, soLuongNuoc: 4, price: 250 };

const gPIZZA_SEAFOOD = { loaiPizza: "Seafood", detail: "PIZZA HẢI SẢN XỐT MAYONNAISE" };
const gPIZZA_HAWAIIAN = { loaiPizza: "Hawaii", detail: "PIZZA DĂM BÔNG DỨA KIỂU HAWAII" };
const gPIZZA_BACON = { loaiPizza: "Bacon", detail: "PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI" };

var order = {
    kichCo: "",
    duongKinh: 0,
    suon: 0,
    salad: 0,
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: 0,
    hoTen: "",
    thanhTien: 0,
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: ""
}

var discount = 0;

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
function changeColorButtonCombo() {
    $("#combo button").removeClass("btn-success").addClass("bg-main");
    $(this).addClass("btn-success").removeClass("bg-main");

    switch ($(this).prop("id")) {
        case "btn-small":
            gCOMBO_SELECTED = gCOMBO_SMALL;
            break;

        case "btn-medium":
            gCOMBO_SELECTED = gCOMBO_MEDIUM;
            break;

        case "btn-large":
            gCOMBO_SELECTED = gCOMBO_LARGE;
            break;
    }

    console.log(gCOMBO_SELECTED);
}

function changeColorButtonPizza() {
    $("#pizza button").removeClass("btn-success").addClass("bg-main");
    $(this).addClass("btn-success").removeClass("bg-main");

    switch ($(this).prop("id")) {
        case "btn-seafood":
            gPIZZA_SELECTED = gPIZZA_SEAFOOD;
            break;

        case "btn-hawaiian":
            gPIZZA_SELECTED = gPIZZA_HAWAIIAN;
            break;

        case "btn-bacon":
            gPIZZA_SELECTED = gPIZZA_BACON;
            break;
    }

    console.log(gPIZZA_SELECTED);
}

function loadDrinkSelect(paramList) {
    for (const key in paramList) {
        $("<option>")
            .val(paramList[key].maNuocUong)
            .text(paramList[key].tenNuocUong)
            .appendTo("#select-drink");
    }
}

function showOrder(paramOrder) {
    $("#inp-order-name").val(paramOrder.hoTen);
    $("#inp-order-phone").val(paramOrder.soDienThoai);
    $("#inp-order-address").val(paramOrder.diaChi);
    $("#inp-order-message").val(paramOrder.loiNhan);
    $("#inp-order-voucher").val(paramOrder.idVourcher);
    $("#area-info-detail")
        .html("Xác nhận: " + paramOrder.hoTen + "; " + paramOrder.soDienThoai + "; " + paramOrder.diaChi
            + "\nCombo: " + paramOrder.kichCo
            + "\nPizza: " + paramOrder.loaiPizza + "; Giá: " + gCOMBO_SELECTED.price + ".000 VNĐ"
            + "\nMã giảm giá: " + paramOrder.idVourcher
            + "\nPhải thanh toán: " + paramOrder.thanhTien + ".000 VNĐ (Giảm giá " + discount + "%)");

    // hiển thị modal lên
    $("#detail-modal").modal("show");
}

function thankYou(res) {
    $("#detail-modal").modal("hide");
    $("#thankyou-modal").modal("show");
    $("#inp-order-code").val(res.orderCode);
}

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function sendClick() {
    getFormOrder(); //thu thap du lieu khi khach an gui
    if (checkInfor(order)) //validate du lieu
        showOrder(order);
}

function confirmClick() {
    order.thanhTien *= 1000;
    $.ajax({
        url: vBASE_URL + "/orders",
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(order),
        success: thankYou //Lấy được orderCode trả về và HIỂN THỊ RA XUỐNG DƯỚI CHO USER        
    });
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function apiGetDrinkList() {
    "use strict";
    $.ajax({
        url: vBASE_URL + "/drinks",
        type: "GET",
        success: loadDrinkSelect,
        error: function (params) { alert(params.status); }
    });
}

function getFormOrder() {
    order.kichCo = gCOMBO_SELECTED.kichCo;
    order.duongKinh = gCOMBO_SELECTED.duongKinh;
    order.suon = gCOMBO_SELECTED.suon;
    order.salad = gCOMBO_SELECTED.salad;
    order.loaiPizza = gPIZZA_SELECTED.loaiPizza;
    order.idVourcher = $("#inp-voucher-id").val().trim();
    order.idLoaiNuocUong = $("#select-drink").val();
    order.soLuongNuoc = gCOMBO_SELECTED.soLuongNuoc;
    order.hoTen = $("#inp-fullname").val().trim();
    order.thanhTien = checkVoucher(order.idVourcher);
    order.email = $("#inp-email").val().trim();
    order.soDienThoai = $("#inp-dien-thoai").val().trim();
    order.diaChi = $("#inp-dia-chi").val().trim();
    order.loiNhan = $("#inp-message").val().trim();
}

function checkVoucher(paramVoucher) {
    "use strict";
    discount = 0;
    //một số mã đúng để test: 95531, 81432,...
    //nếu mã giảm gia đã nhập, tạo xmlHttprequest và gửi về server
    if (paramVoucher != "")
        $.ajax({
            url: vBASE_URL + "/voucher_detail/" + paramVoucher,
            type: "GET",
            async: false,
            success: function (res) {
                discount = res.phanTramGiamGia;
            }
        });

    return gCOMBO_SELECTED.price * (100 - discount) / 100;
}

function checkInfor(paramOrder) {
    if (paramOrder.kichCo == "")
        alert("Choose a Menu");
    else if (paramOrder.loaiPizza == "")
        alert("Choose a Pizza");
    else if (paramOrder.idLoaiNuocUong == "NO_DRINK")
        alert("Choose a Drink");
    else if (paramOrder.idVourcher != "" && paramOrder.thanhTien >= gCOMBO_SELECTED.price)
        alert("Voucher not found");
    else if (paramOrder.hoTen == "" || paramOrder.email == "" || paramOrder.soDienThoai == "" || paramOrder.diaChi == "")
        alert("Information is empty");
    else if (checkEmail(paramOrder.email) && checkPhoneNumber(paramOrder.soDienThoai))
        return true;

    return false;
}

function checkEmail(paramEmail) {
    if (!paramEmail.includes("@") || paramEmail.startsWith("@") || paramEmail.endsWith("@"))
        alert("Email is wrong");
    else
        return true;

    return false;
}

function checkPhoneNumber(paramNumber) {
    if (isNaN(paramNumber)) {
        alert("Số điện thoại không đúng");
        return false;
    }

    return true;
}