"use strict";
$(document).ready(function () {
    /// 2 - C: Create
    $("#btn-add-new").click(onBtnAddNewClick);
    // 3 - U: Update
    $("#table-orders").on("click", ".edit-btn", onBtnEditClick);
    // 4 - D: Delete
    $("#table-orders").on("click", ".delete-btn", onBtnDeleteClick);

    // gán sự kiện cho nút Create (trên modal)
    $("#btn-create").click(onBtnCreateClick);
    // gán sự kiện cho nút Update (trên modal)
    $("#btn-update").click(onBtnUpdateClick);
    // gán sự kiện cho nút Confirm (trên modal)
    $("#btn-confirm-delete").click(onBtnConfirmClick);
})

/* CREATE CASE */
// Hàm xử lý sự kiện khi nút Thêm mới đc click
function onBtnAddNewClick() {
    // hiển thị modal trắng lên
    $("#create-modal").modal("show");
}

// hàm xử lý sự kiện create modal click
function onBtnCreateClick() {
    // khai báo đối tượng chứa order data
    var vObjectRequest = {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen: "",
        thanhTien: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: ""
    };
    // B1: Thu thập dữ liệu
    getCreateOrderData(vObjectRequest);
    // B2: Validate insert
    var vIsValidate = validateOrderData(vObjectRequest);
    if (vIsValidate) {
        // B3: API tạo (Create) một grade mới
        $.ajax({
            url: gBASE_URL + "/orders",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(vObjectRequest),
            success: handleCreateSuccess, // B4: xử lý front-end
            error: function (paramErr) { alert(paramErr.status); }
        });
    }
}

// hàm thu thập dữ liệu để create order
function getCreateOrderData(paramObj) {
    paramObj.kichCo = $("#sel-create-combo").val();
    paramObj.duongKinh = $("#inp-create-dk").val();
    paramObj.suon = $("#inp-create-suon").val();
    paramObj.salad = $("#inp-create-salad").val();
    paramObj.soLuongNuoc = $("#inp-create-number").val();
    paramObj.thanhTien = $("#inp-create-price").val();
    paramObj.loaiPizza = $("#sel-create-pizza").val();
    paramObj.idVourcher = $("#inp-create-voucher").val();
    paramObj.idLoaiNuocUong = $("#sel-create-drink").val();
    paramObj.hoTen = $("#inp-create-name").val().trim();
    paramObj.email = $("#inp-create-email").val().trim();
    paramObj.soDienThoai = $("#inp-create-phone").val();
    paramObj.diaChi = $("#inp-create-address").val().trim();
    paramObj.loiNhan = $("#inp-create-message").val().trim();
}

// hàm xử lý hiển thị front-end khi thêm thành công
function handleCreateSuccess() {
    alert("Thêm thành công!");
    apiGetAllOrders();
    $("#create-modal").modal("hide");
}

/* UPDATE CASE */
// Hàm xử lý sự kiện khi icon edit trên bảng đc click
function onBtnEditClick() {
    getDataFromButton(this);
    // load dữ liệu vào các trường dữ liệu trong modal
    showDataToModal(orderSelected);
    // hiển thị modal lên
    $("#update-modal").modal("show");
}

// hàm show order obj lên modal
function showDataToModal(paramOrder) {
    $("#inp-update-code").val(paramOrder.orderCode);
    $("#inp-update-combo").val(paramOrder.kichCo);
    $("#inp-update-pizza").val(paramOrder.loaiPizza);
    $("#inp-update-drink").val(paramOrder.idLoaiNuocUong);
    $("#inp-update-price").val(paramOrder.thanhTien);
    $("#inp-update-voucher").val(paramOrder.idVourcher);
    $("#inp-update-discount").val(paramOrder.giamGia);
    $("#inp-update-name").val(paramOrder.hoTen);
    $("#inp-update-email").val(paramOrder.email);
    $("#inp-update-phone").val(paramOrder.soDienThoai);
    $("#inp-update-address").val(paramOrder.diaChi);
    $("#inp-update-message").val(paramOrder.loiNhan);
    $("#sel-update-status").val(paramOrder.trangThai);
}

// hàm xử lý sự kiện update modal click
function onBtnUpdateClick() {
    // khai báo đối tượng chứa update data
    var vObjectRequest = {
        trangThai: $("#sel-update-status").val() //3 trang thai open, confirmed, cancel
    };
    // update status
    $.ajax({
        url: gBASE_URL + "/orders/" + orderSelected.id,
        type: "PUT",
        contentType: "application/json",
        data: JSON.stringify(vObjectRequest),
        success: handleUpdateSuccess, // B4: xử lý front-end
        error: function (paramErr) { alert(paramErr.status); }
    });
}

// hàm xử lý hiển thị front-end khi sửa thành công
function handleUpdateSuccess() {
    alert("Update status thành công!");
    apiGetAllOrders();
    $("#update-modal").modal("hide");
}

/* DELETE CASE */
// Hàm xử lý sự kiện khi icon delete trên bảng đc click
function onBtnDeleteClick() {
    getDataFromButton(this);
    // hiển thị modal lên
    $("#delete-confirm-modal").modal("show");
}

// hàm xử lý sự kiện confirm modal click
function onBtnConfirmClick() {
    // delete order
    $.ajax({
        url: gBASE_URL + "/orders/" + orderSelected.id,
        type: "DELETE",
        success: handleDeleteSuccess, // xử lý front-end
        error: function (paramErr) { alert(paramErr.status); }
    });
}

// hàm xử lý hiển thị front-end khi delete thành công
function handleDeleteSuccess() {
    alert("Delete thành công!");
    apiGetAllOrders();
    $("#delete-confirm-modal").modal("hide");
}

// hàm dựa vào button detail (edit or delete) xác định đc data
function getDataFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    orderSelected = vOrdersTable.row(vTableRow).data();
}

// hàm validate data
function validateOrderData(paramOrder) {
    if (paramOrder.hoTen == "" || paramOrder.soDienThoai == "" || paramOrder.diaChi == "")
        alert("Information is empty");
    else if (!checkPhoneNumber(paramOrder.soDienThoai))
        alert("Phone is wrong");
    else if (paramOrder.idVourcher != "" && checkVoucher(paramOrder.idVourcher) == 0)
        alert("Voucher not found");
    else if (paramOrder.email != "" && !checkEmail(paramOrder.email))
        alert("Email is wrong");
    else
        return true;

    return false;
}

function checkPhoneNumber(paramNumber) {
    if (!paramNumber.startsWith("0") || paramNumber.length != 10 || paramNumber.includes("."))
        return false
    else
        return true;
}

function checkVoucher(paramVoucher) {
    "use strict";
    var discount = 0;
    $.ajax({
        url: gBASE_URL + "/voucher_detail/" + paramVoucher,
        type: "GET",
        async: false,
        success: function (res) { discount = res.phanTramGiamGia; }
    });

    return discount;
}

function checkEmail(paramEmail) {
    if (!paramEmail.includes("@") || paramEmail.startsWith("@") || paramEmail.endsWith("@"))
        return false;
    else
        return true;
}